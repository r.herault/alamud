# -*- coding: utf-8 -*-
# Copyright (C) 2018 Guillaume/Romain, IUT d'Orléans
#==============================================================================

from .event import Event2

class EatEvent(Event2):
    NAME = "eat"

    def perform(self):
        if not self.object.has_prop("eatable"):
            self.add_prop("object-not-eatable")
            return self.eat_failed()
        if self.object in self.actor:
            self.add_prop("object-already-eat")
            return self.eat_failed()
        self.object.move_to(None)
        self.inform("eat")

    def eat_failed(self):
        self.fail()
        self.inform("eat.failed")
