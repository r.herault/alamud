# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2


class FireOnEvent(Event2):
    NAME = "fire-on"

    def perform(self):
        if not self.object.has_prop("fireable"):
            self.fail()
            return self.inform("fire-on.failed")
        self.inform("fire-on")


class FireOffEvent(Event2):
    NAME = "fire-off"

    def perform(self):
        if not self.object.has_prop("fireable"):
            self.fail()
            return self.inform("fire-off.failed")
        self.inform("fire-off")
