# -*- coding: utf-8 -*-
# Copyright (C) 2014 HERAULT Romain, IUT d'Orléans
#==============================================================================

from .action import Action2
from mud.events import FireOnEvent, FireOffEvent

class FireOnAction(Action2):
    EVENT = FireOnEvent
    ACTION = "fire-on"
    RESOLVE_OBJECT = "resolve_for_use"

class FireOffAction(Action2):
    EVENT = FireOffEvent
    ACTION = "fire-off"
    RESOLVE_OBJECT = "resolve_for_use"
